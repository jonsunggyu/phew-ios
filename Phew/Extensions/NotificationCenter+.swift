//
//  NotificationCenter+.swift
//  Phew
//
//  Created by Ahmed Elesawy on 1/24/21.
//  Copyright © 2021 Mohamed Akl. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let reloadHomeData = NSNotification.Name("reloadHomeData")
}
