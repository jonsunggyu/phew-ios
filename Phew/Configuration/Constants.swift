//
//  Constants.swift
//  Phew
//
//  Created by Youssef on 9/3/20.
//  Copyright © 2020 Mohamed Akl. All rights reserved.
//

import Foundation

struct Constants {
//    private(set) static var baseUrl: String = "https://phew.orabi.rmal.com.sa/api/"
    private(set) static var baseUrl: String = "https://www.phewdashboard.com/api/"
    private(set) static var moviFilmKey: String = "b9aa09eb38643436e7f8e12a1ba2e953"
    private(set) static var googleMapKey: String = "AIzaSyDRymdCLWxCwLHFnwv36iieKAMjiwk8sdc"
    private(set) static var socketUrl: String = "https://phew.orabi.rmal.com.sa:6061"
}
