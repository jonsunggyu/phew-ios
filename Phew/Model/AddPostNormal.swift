//
//  AddPostNormal.swift
//  Phew
//
//  Created by Ahmed Elesawy on 11/16/20.
//  Copyright © 2020 Mohamed Akl. All rights reserved.
//

import Foundation
import UIKit



struct AddPostNormalModel {
    let image:UIImage?
    let type:HomePostNormalTypeEnum
    var id: Int? 
    
}
